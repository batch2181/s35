const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;


app.use(express.json());


app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin123@batch218-to-do.maumm35.mongodb.net/User", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    status:{
        type: String,
        default: "pending"
    }
});

const User = mongoose.model("User", userSchema);


app.post("/signup", (req, res) => {
    console.log("username: " + req.body.username);
    console.log("password: " + req.body.password);
    User.findOne({username: req.body.username}, (err, result) => {
        if(result != null && result.username == req.body.username){
            return res.send("Duplicate user found!");
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((savedErr, savedUser) => {
                if(savedErr){
                    return console.error(savedErr)
                } else {
                    return res.status(201).send("New user registered.")
                }
            })
        }
    })
})






app.listen(port, () => console.log(`Server is running at port ${port}`));
